(function ($) {

  Drupal.behaviors.DisclaimerLocalStorage = {
    attach: function (context, settings) {
			var cookie = "disclaimer";
			$('.disclaimer-close').click(function() {
				localStorage.setItem('disclaimer', cookie);
				$('.block-disclaimer-message').fadeTo(300,0,function(){
					$(this).remove();
				});
			});
			if(localStorage.getItem(cookie) == cookie) {
				$(".block-disclaimer-message").hide();
			}
    }
  }
})(jQuery);
