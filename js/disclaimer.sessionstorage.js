(function ($) {

  Drupal.behaviors.DisclaimerMessage = {
    attach: function (context, settings) {
			var cookie = "disclaimer";
			$('.disclaimer-close').click(function() {
				sessionStorage.setItem('disclaimer', cookie);
				$('.block-disclaimer-message').fadeTo(300,0,function(){
					$(this).remove();
				});
			});
			if(sessionStorage.getItem(cookie) == cookie) {
				$(".block-disclaimer-message").hide();
			}
    }
  }
})(jQuery);
