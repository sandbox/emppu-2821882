<?php

/**
 * @file
 * Admin page callbacks for the disclaimer_message module.
 */

/**
 * Disclaimer message configuration page.
 */
function disclaimer_message_admin_settings() {
  $form['disclaimer_message_add_sessionstorage'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('disclaimer_message_add_sessionstorage', 1),
    '#title' => t('Make the disclaimer message to use sessionStorage'),
    '#description' => t('<p>Adds the javascript sessionStorage to the element when it is closed. Message is hidden until user opens a new session in the site</p><a href="@gcf">@gcf</a>', array(
      '@gcf' => url('https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage'),
    )),
  );
  $form['disclaimer_message_add_cookie'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('disclaimer_message_add_cookie', 0),
    '#title' => t('Make the disclaimer message to use localStorage'),
    '#description' => t('<p>Saves the click to localStorage when the disclaimer message is closed. Expires in x'),
  );
  $form['date_title'] = array(
    '#type' => '#markup',
    '#prefix' => '<h2>' . t('Scheduling options'),
    '#suffix' => '</h2>',
  );
  $form['disclaimer_message_start_time'] = array(
        '#type' => 'date_popup',
        '#title' => t('Start'),
        '#description' => t('Select start time when block is visible'),
        '#default_value' => variable_get('disclaimer_message_start_time', time()),  
        '#date_format' => 'Y-m-d H:i', 
  );               
  $form['disclaimer_message_end_time'] = array(
        '#type' => 'date_popup',
        '#title' => t('End'),
        '#description' => t('Select end time when block is hidden'),
        '#default_value' => variable_get('disclaimer_message_end_time', time()),   
        '#date_format' => 'Y-m-d H:i', 
  );
  return system_settings_form($form);
}